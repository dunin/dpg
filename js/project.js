
var updateDisplay = false;

$(function() {
	$("body").delegate( "input", "keypress", function(e) {
		if(e.keyCode == 13) {
			$(this).blur();
			checkChangeDisplay();
			return false;
		}
	});
	
	$("body").on(  "mousedown", ".draggable", dragMouseDown);
	$("body").on(  "change", "#formPerso", savePerso);
	$("body").on(  "change", "#formIntro", saveIntro);
	$("body").on(  "change", "#formQuestion", saveQuestion);
	$("body").on(  "click", ".closeThing", closeThing);

	$("body").on(  "click", ".changeDisplay", changeDisplay);

	$("body").on(  "change", ".changeDisplay", checkChangeDisplay);
	$("body").on(  "click", checkChangeDisplay);
})

function renderProject(idFocus) {
	if(idFocus) idFocus = "#"+idFocus;
	console.log("renderProject",project,"idFocus",idFocus);
	if(dragTimeout) clearTimeout(dragTimeout);
	if(!project) $(document.body).empty();
	else {
		var main="";
		
		switch (project.display) {
			case "perso":
				var perso = project.currentPerso;
				if(perso != "new") perso = _.find(project.persos,{id:parseInt(perso)});
				main = templates["perso"]( {p:perso} ) ;
				idFocus="#nom";
				break;
			case "intro":
				project.currentPerso = null;
				main = templates["introduction"]( {i:project.currentIntro} );
				idFocus = "#corps";
				break;
			case "question":
				if (!project.currentQuestion) project.currentPerso = null;
				var perso = _.find(project.persos,{id:parseInt(project.currentPerso)});

				if(project.currentQuestion =="new") newQuestion();
				var question = _.find(project.questions,{id:parseInt(project.currentQuestion)});
				if(!perso && question && project.persos.length) {
					perso = project.persos[0];
					project.currentPerso = perso.id;
				}
				console.log("question",question," perso",perso);
				main = templates["question"]( {p:perso,q:question,dragging:dragging} );
				if(!idFocus ) idFocus = "#titre";
				break;
		}

		$("body").html(templates["main"](project))
			.find("#main").html(main)
			.find(idFocus).focus();
	}
}

function changeDisplay(e) {
	console.log("changeDisplay");
	if(e) {
		project.display = e.currentTarget.dataset.display;
		if(e.currentTarget.dataset.cible) project[e.currentTarget.dataset.cible] = e.currentTarget.dataset.id;
	}
	updateDisplay = true;
	// return false;
}

function checkChangeDisplay(e) {
	console.log("checkChangeDisplay");
	if(!updateDisplay) return;
	console.log("	TRUE");
	updateDisplay = false;
	renderProject(document.activeElement.id);
	return false;
}

function savePerso() {
	console.log("change perso");
	$form = $(this);
	var id = $form.data("id");
	var nom = $form.find("#nom").val();
	var coupable = $form.find("#coupable").prop("checked");

	if(!nom) {
		if(confirm (T.confirmeDelete)) {
			project.persos = _.reject(project.persos,{id:id});
			project.display = null;
			project.currentPerso = null;
			changeDisplay();
			// return false;
		} else {
			$form.find("#nom").focus().val("?");
			return;
		}
	}

	if(id == "new") {
		var newId = 1;
		while(_.find(project.persos,{id:newId})) newId++;
		project.persos.push({id:newId,coupable:coupable});
		id=newId;
	}

	var perso = _.find(project.persos,{id:id});
	project.currentPerso = perso.id;
	project.saved = false;
	perso.nom = nom;


	if(perso.coupable != coupable) {
		// on vient de changer la checkbox donc il n'y aura pas d'autre events, et il faut changer aussi l'affichage side
		perso.coupable = coupable;
		// return false;
	}
	
	changeDisplay();
	// return false;
}

function saveIntro() {
	$form = $(this);
	var id = $form.data("id");
	var titre = $form.find("#titre").val();
	var corps = $form.find("#corps").val();

	var intro = project[id];
	intro.titre = titre;
	intro.corps = corps;
	project.currentIntro = null;

	project.saved = false;
	changeDisplay();
	// return false;
}

function newQuestion() {
	var newId = 1;
	while(_.find(project.questions,{id:newId})) newId++;
	project.questions.push({id:newId,titre:T.whatIs,rep:[],hide:false});
	project.currentQuestion = newId;
}

function saveQuestion() {
	console.log("save question");
	$form = $(this);
	var id = $form.data("id");
	var titre = $form.find("#titre").val();
	var hide = $form.find("#hide").prop("checked");

	if(!titre) {
		if (confirm (T.confirmeDelete)) {
			project.questions = _.reject(project.questions,{id:id});
			project.currentQuestion = null;
			changeDisplay();
			// return false;
		} else {
			$form.find("#titre").focus().val("?");
			return;
		}
	}

	var question = _.find(project.questions,{id:id});
	question.titre = titre;
	question.hide = hide;
	

	// parsing des réponses
	$verite = $form.find("#verite");
	if($verite[0]) {
		var reponse = _.find(question.rep,{perso:$verite.data('id')});
		if(!reponse) {
			reponse = {perso:$verite.data('id')};
			question.rep.push(reponse);
		}
		reponse.verite = $verite.val();
		reponse.unlockVerite = $form.find("#unlockVerite").val();

		$mensonge = $form.find("#mensonge");
		if($mensonge[0]) {
			reponse.mensonge = $mensonge.val();
			reponse.unlockMensonge = $form.find("#unlockMensonge").val();
		}
	}

	project.saved = false;
	changeDisplay();
	return true;
}

var $dragEl,dragTimeout,dragging,dragOffset,$dragDest;
function dragMouseDown(e) {
	if(project.currentQuestion) return;
	$dragEl = $(this);
	dragOffset = {x:(e.originalEvent.pageX - $dragEl.offset().left),y: (e.originalEvent.pageY - $dragEl.offset().top) };
		
	dragTimeout = setTimeout(function(){startDrag(e)},500);
	$("body").on("mouseup",stopDragEl);
}

function startDrag(e) {
	dragTimeout=null;
	dragging=true;
	project.currentQuestion = $dragEl.data("id");
	$dragEl.css({width:$("#main").width(),height:$dragEl.height()});
	renderProject();

	$("body").append($dragEl);
	$dragEl.addClass("dragging");
	$dragEl.removeClass("drag");

	$dragDest = $(".draggable[data-id="+project.currentQuestion+"]");

	$("body").on("mousemove",moveDragEl);
	moveDragEl (e);
	// $dragEl.once("mouseup",stopDragEl);
}

function moveDragEl (e) {
	$dragEl.css({left:e.originalEvent.pageX-dragOffset.x,top:e.originalEvent.pageY-dragOffset.y});

	$(".draggable").each(function(id,el) {
		var $el = $(this);
		var top = $el.offset().top;
		console.log("e.originalEvent.pageY",e.originalEvent.pageY,"top",top);
		if(e.originalEvent.pageY>top && e.originalEvent.pageY<top+$el.innerHeight()) {
			console.log("change!");
			if(top<$dragDest.offset().top) $el.before($dragDest);
			else $el.after($dragDest);
		}
	});
}

function stopDragEl (e) {
	$("body").off("mouseup",stopDragEl);
	if(dragTimeout) clearTimeout(dragTimeout);
	else {
		$("body").off("mousemove",moveDragEl);
		dragging=false;
		$(".draggable").each(function(id,el) {
			var q = _.find(project.questions,{id:parseInt(el.dataset.id)});
			q.order = id;
		});
		project.questions = _.sortBy(project.questions, 'order');
		project.saved = false;
		project.currentQuestion = null;
		changeDisplay();
		return false;
	}
}

function closeThing(e) {
	console.log("closeThing",e);
	project[e.target.dataset.toclose] = null;
	changeDisplay();
}
