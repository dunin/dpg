async function createRules(resolve) {
	// in rules there are images next to example text
		var txt = fs.readFileSync("rules/"+lg+".md");
		var testColumns;
		var rulesTab = [];
		while(testColumns = (/([^]*?)^(.*?)\|(.*)$([^]*)?/m).exec(txt) ) {
			// on cherhce les colonnes texte|illustration
			var imageData = await imgToDataURL(testColumns[3]);

			// s'il y a du texte avant
			if(testColumns[1] && testColumns[1].length) rulesTab.push({text:mdText(testColumns[1]),style:"corps"});

			// la fameuse colonne
			var col = {columnGap:20, columns:[
					{text: mdText(testColumns[2]), width: 'auto',style:'exemple', headlineLevel: 1 }
				]};
			if (imageData) col.columns.push({image: imageData, width: 350 });
			rulesTab.push(col);

			// s'il y a un reste, il faut le tester
			txt = testColumns[4]||"";
		}

		// le reste qui n'a pas passer le test text|illustration
		rulesTab.push({text:mdText(txt),style:"corps"});

		rulesTab[0].pageBreak = 'before';

		return(rulesTab);
}

function mdText (txt,modif,style) {
	// really ugly simplify-fake-markdown

	var headerParse = (/([^]*?)^(#{1,2}) (.*)$([^]*)?/m).exec(txt);
	// console.log("headerParse",headerParse);
	if(headerParse != null) {
		console.log("headerParse",headerParse);
		var el=[];
		var headerType = (headerParse[2].length==1)?'bigHeader':'header';
		if(headerParse[1] && headerParse[1].length) el.push({text:mdText(headerParse[1])});
		el.push({text:mdText(headerParse[3],null,headerType),style:[headerType]});
		if(headerParse[4] && headerParse[4].length) el.push({text:mdText(headerParse[4])});
		return el;
	}
	
	var boldParse = (/([^]*?)(\*\*|__)([^]*?)(\*\*|__)([^]*)/).exec(txt);
	var itParse = (/([^]*?)(_|\*)([^]*?)(_|\*)([^]*)/).exec(txt);
	var el;

	if(boldParse==null && itParse == null) return txt;

	else if(itParse==null || (boldParse != null && boldParse[1].length<= itParse[1].length)) {
		el = [{text:boldParse[1]},{text:mdText(boldParse[3],"bold"),bold:true},{text:mdText(boldParse[5])}];
	}
	else el = [{text:itParse[1]},{text:mdText(itParse[3],"italics"),italics:true},{text:mdText(itParse[5])}];

	if(modif) el = el.map(function(e){
		e[modif] = true;
		return e;
	});

	if(style) el = el.map(function(e){
		e.style = (e.style)?e.style.concat(style):[style];
		return e;
	});

	return el;
}



function imgToDataURL (src) {
	return new Promise(function (resolve, reject) {
	    var image = $('<img/>').on("load",function(e){
	    	var el = e.currentTarget;
			var canvas = document.createElement("canvas");
			canvas.width = el.naturalWidth;
			canvas.height = el.naturalHeight;
			canvas.getContext('2d').drawImage(el,0,0);

			resolve(canvas.toDataURL());
	    })
	    .on('error', function() { resolve(null); })
	    .attr("src", "img/"+src);
	    console.log("image");

	});
}
