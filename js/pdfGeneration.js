async function generatePdf(firstPages,addRules) {
	firstPages = (firstPages)? firstPages : [];

	if(addRules) {
		firstPages = firstPages.concat (await createRules());
	}

	var reponses = project.questions.reduce(function(acc,el) {
		var r = el.rep.reduce(function(accR,elR){
			// on enlève les paragraphes vides
			if(!elR.verite && !elR.mensonge) return accR;
			elR.qId = el.id;

			elRClone = _.clone(elR);
			
			if(elRClone.verite) elRClone.verite = testUnlock(elRClone.verite,elRClone.unlockVerite)
			if(elRClone.mensonge) elRClone.mensonge = testUnlock(elRClone.mensonge,elRClone.unlockMensonge)

			accR.push(elRClone);
			// si il y a verite + mensonge, il faut deux paragraphes de réponse
			if(elR.verite && elR.mensonge) accR.push(_.clone(elRClone));
			return accR;
		},[]);
		 
		return acc.concat(r);
	},[]);

	reponses = _.shuffle(reponses);

	console.log("firstPages",firstPages);

	return {
		info: {
			creator : (project.settings.couvAuteur)?project.settings.couvAuteur:"",
			producer : (project.settings.couvAuteur)?project.settings.couvAuteur:""
		 },
		content:firstPages.concat([
			{ text: mdText(project.introFlic.titre||T.inspectorIntroduction), style:'header',pageBreak: (firstPages.length)?'before':undefined },
			{ text: mdText(project.introFlic.corps), style:"corps" },
			{ text: T.inspectorTable, style:'header',pageBreak: 'before' },
		    generateTable(false,reponses),
			{ text: mdText(project.introCoupable.titre||T.culpritIntroduction), style:'header' },
			{ text: mdText(project.introCoupable.corps), style:"corps" },
			
		    { text: T.suspectsTable, style:'header',pageBreak: 'before' },
		    generateTable(true,reponses),
		    generateParagraphes(reponses)
		  ]),

	   // footer: function(currentPage, pageCount) { return {text:currentPage.toString(), alignment: 'right', margin:5} },

	   styles: {
	   		bigHeader: {
	   			fontSize: 18,
				bold: true,
				margin: [0, 20]
	   		},
			header: {
				fontSize: 14,
				bold: true,
				// margin: [left, top, right, bottom]
				// or margin: [horizontal, vertical]
				margin: [0, 20]
			},
			corps: {
				fontSize: 10
			},
			exemple: {
				fontSize: 8
			}
		},

		pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
			console.log("currentNode",currentNode);
		 return currentNode.headlineLevel === 1 && 
		 		(followingNodesOnPage.length === 0
		 		|| (followingNodesOnPage.length === 1 && followingNodesOnPage[0].pageNumbers.length>1) );
		}
	};
}

function testUnlock (text, unlockId)  {
	console.log("testUnlock","text",text,"unlockId",unlockId);
	if(unlockId) {
		// si il y a un déblocage, il faut l'afficher
		var qUnlock = _.find(project.questions,{id:parseInt(unlockId)});
		if(qUnlock) text += "\n\n_"+(project.settings.unlock || T.unlockSentence)+"\n"+qUnlock.titre+"_";
	}
	return text;
}

function generateTable(coupable,reponses) {
	var persos = _.shuffle(project.persos);

	var header = [{border:[false,false,false,false],text:"",colSpan:2},{}].concat(persos.map(function(el){
		return generateVerticalTitres([el.nom],_.pluck(persos,"nom"),14, coupable&& el.coupable); 
	}));

	var widths = persos.reduce(function(acc,el){
		if(coupable && el.coupable) return acc.concat([40]);
		return acc.concat([20]);
	},['auto','*']);

	var layout= {
					hLineWidth: function (i, node) {
						return (i === 0) ? 0 : 1;
					},
					vLineWidth: function (i, node) {
						return 1;
					},
					hLineColor: function (i, node) {
						return 'grey';
					},
					vLineColor: function (i, node) {
						return 'grey';
					}
				};
	var corps = createTableCorps(coupable,persos,reponses);

	return {table:{
				widths: widths,
				body: [header].concat(corps)
			},
		    layout:layout,
		    pageBreak: 'after'
		    }
}

function generateVerticalTitres(txt,txtTab,fontHeight,double) {
	var scale = 10; // meilleure résolution
	colWidth = fontHeight = (fontHeight)? fontHeight*scale: 16*scale;
	if(double) colWidth = colWidth*2;
	var rotationAngle = -Math.PI/2.5;
	var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    ctx.font = fontHeight+"px Arial";
    var metrics, maxWidth = 0;
    var rapportLineHeight = 1.618; // rapport font height/line height
    // rapportLineHeight = 1.8;

    for (var i=0;i<txtTab.length;i++) {
    	metrics = ctx.measureText(txtTab[i]);
    	if(maxWidth<metrics.width) maxWidth = Math.ceil(metrics.width);
    }
    metrics = ctx.measureText(txt);

    canvas.width = colWidth+Math.cos(rotationAngle)*Math.ceil(metrics.width);
    canvas.height = Math.sin(-rotationAngle)*maxWidth + Math.sin(-rotationAngle)*fontHeight;

    var offsetX = colWidth/2 + (Math.cos(rotationAngle)*fontHeight*rapportLineHeight) /2;
    ctx.imageSmoothingEnabled = false;

    ctx.clearRect(0,0,canvas.width,canvas.height);
	ctx.font = fontHeight+"px Arial";
	ctx.translate(offsetX,canvas.height);
	ctx.rotate(rotationAngle);
	ctx.fillText(txt,0,0);


	return {image:canvas.toDataURL("image/png"),
	width:canvas.width/scale,
	border: [false, false, false, false]/*,alignment:(double)?"center":"left"*/};
};


function createTableCorps(coupable,persos,reponses) {
	console.log("###############################");
	console.log("GENERATE TABLE FOR ",(coupable)?"COUPABLE":"DETECTIVE");
	var cross = {text:"X",alignment:"center"};
	var vide = ' ';
	return project.questions.map(function(q,id) {
		var cases = [];
		console.log("question : ",q.id)

		for (var i=0;i<persos.length;i++) {
			console.log("	perso : ",persos[i].id)
			var rep = _.findWhere(reponses,{perso:persos[i].id,qId:q.id});
			console.log("		rep",rep);

			if(coupable && persos[i].coupable) {
				console.log("		COUPABLE");
				rep = _.clone(rep); // parce qu'on va modifier les propriétés de l'objet original
				var repsVetM = _.where(reponses,{perso:persos[i].id,qId:q.id});
				var verite,mensonge;

				console.log("				repsVetM",_.clone(repsVetM));
				if(rep && rep.verite) {
					var repV = repsVetM.shift();
					repV.mensonge = null;
					verite = (reponses.indexOf(repV)+1)+"";
				} else verite = "X";
				if(rep && rep.mensonge) {
					var repM = repsVetM.shift();
					repM.verite = null;
					mensonge = (reponses.indexOf(repM)+1)+"";
				} else mensonge = "X";

				// affichage special deux colones
				cases.push({
					table:{
						body:[[
							{text:verite,alignment:"center"},
							{text:mensonge,alignment:"center",color:"grey"}
							]],
						widths:["*", "*"]
					},
					layout:"noBorders"
				});
			} else {
				if(!rep) cases.push(_.clone(cross));
				else if(coupable) cases.push({text:(reponses.indexOf(rep)+1)+"",alignment:"center"});
				else cases.push(vide);
			}

		}

		return [
			{text:(id+1)+"",alignment:"right"},
			(!q.hide || coupable)? q.titre : vide
		].concat(cases);
	});
}


function generateParagraphes(reponses)  {
	var paragraphes = reponses.reduce(function(acc,el,id){
		return acc.concat([{text: (id+1)+'',alignment: 'center',style:"header", headlineLevel: 1}, {text:mdText((el.verite||el.mensonge)+"\n\n"),style:"corps", paragraphe:1}]);
	},[])

	return {
          stack: paragraphes
        }
}
