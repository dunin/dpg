var fs = require('fs');
var path=require('path');
var win = nw.Window.get();
var lgFile = path.join(nw.App.dataPath, "lang");
var lg = (fs.existsSync(lgFile))? fs.readFileSync(lgFile) : "fr";
var lgDispos = ["fr","en"];
var templates =[];

var T = JSON.parse(fs.readFileSync("lang/"+lg+".json"));
T.tips = _.shuffle(T.tips);

console.log("nw.App.dataPath",nw.App.dataPath);

// Create an empty menubar
var menu = new nw.Menu({type: 'menubar'});
var project = null;
var exporting = false;

// Create a submenu as the 2nd level menu
var submenuLoad = new nw.Menu();
fillLoadMenu();

// Create and append the 1st level menu to the menubar
menu.append(new nw.MenuItem({
  label: T.new,
  click: newProject
}));

var menuCharger = new nw.MenuItem({
  label: T.load,
  submenu: submenuLoad
});

var menuSave = new nw.MenuItem({
  label: T.saveAs,
  click: saveAs,
  enabled:false
});
var menuSaveAs = new nw.MenuItem({
  label: T.save,
  click: save,
  enabled:false
});

var menuRemove = new nw.MenuItem({
  label: T.delete,
  click: supp,
  enabled:false
});

var menuExport = new nw.MenuItem({
  label: T.export,
  click: exportProject,
  enabled:false
});

// menu langue
var submenuLg = new nw.Menu();

fillSubmenuLg();

var menuLg = new nw.MenuItem({
  label: lg,
  submenu: submenuLg
});


menu.append(menuCharger);
menu.append(menuSave);
menu.append(menuSaveAs);
menu.append(menuRemove);
menu.append(menuExport);
menu.append(menuLg);

// Assign it to `window.menu` to get the menu displayed


function testClose(customMessage) {
  if(project && !project.saved && !confirm(customMessage||T.notSave)) return false;
    return true;
}

function saveAs () {
  if(project == null) return;
  project.fileName = prompt(T.chooseAName,T.noName);
  if(project.fileName 
    &&
    (!fs.existsSync(path.join(nw.App.dataPath, project.fileName+".json")) || confirm(T.nameUsed)) ) save();
}

function save () {
  if(project == null) return;
  if(!project.fileName) return saveAs();
  var pathToSave = path.join(nw.App.dataPath, project.fileName+".json");
	fs.writeFileSync(pathToSave,JSON.stringify(project), function(e) {
			if(e) alert(T.failedToSave+e);
	});
  project.saved = true;
  fillLoadMenu();
}

function load (projectName) {
  if (!testClose()) return;
  project = JSON.parse(fs.readFileSync(path.join(nw.App.dataPath, projectName+".json")));
  if(project.settings == undefined) project.settings = {};
  project.saved = true;
  menuSave.enabled = true;
  menuSaveAs.enabled = true;
  renderProject();
}

function newProject () {
  if (!testClose()) return;
  project = {"settings":{},"introFlic":{"titre":"","corps":""},"introCoupable":{"titre":"","corps":""},"persos":[],"questions":[],"saved":true};
  menuSave.enabled = true;
  menuSaveAs.enabled = true;
  menuExport.enabled = true;
  renderProject();
}

function fillLoadMenu () {
  var dir = fs.readdirSync(nw.App.dataPath);
  while(submenuLoad.items.length) submenuLoad.removeAt(0);

  dir.forEach(function(el) {
    el = el.match(/^(.*)?\.json$/i);
    if(el != null && el[1]) submenuLoad.append(new nw.MenuItem({ label: el[1], click:loadFromMenu }));
  })
}

function loadFromMenu (e) {
  load(this["label"]);
}

function supp() {
  if(!project) return;
  if(project.fileName) {
    if(!confirm(T.confirmDeleteP)) return;
    fs.unlinkSync(path.join(nw.App.dataPath, project.fileName+".json"));
    project = null;
  }

  renderProject();
  fillLoadMenu();
}

function exportProject() {
  if(!project) return;
  setMenuExport();
}

win.on("close",function() {
  if (!testClose()) return;
  if(exporting && !confirm(T.exportNotEnd)) return;
  this.close(true);
});

function fillSubmenuLg () {
  var dir = fs.readdirSync("lang/");

  dir.forEach(function(el) {
    el = el.match(/^(.*)?\.json$/i);
    if(el != null && el[1]) submenuLg.append(new nw.MenuItem({ label: el[1], click:changeLangue }));
  })
}

function changeLangue () {
  if (!testClose(T.toChangeLg)) return;
  lg = this["label"];
  fs.writeFileSync(lgFile,lg);
  win.reload();
}


$(function() { 
  win.menu = menu;
  templates = {
    welcome:_.template($("#welcome").html()),
    main:_.template($("#template").html()),
    perso: _.template($("#persos").html()),
    introduction: _.template($("#introduction").html()),
    question: _.template($("#question").html()),
    exportSettings: _.template($("#exportSettings").html()),
    exportEnCours: _.template($("#exportEnCours").html())
  }

  setWelcome();
  $("body").on("click","#anotherTip",setWelcome);
});

function setWelcome() {
  $("body").html(templates["welcome"]());
  T.tips.push(T.tips.shift());
}
