$(function() {
	$("body").delegate("#couvImg", "change", setImgCouv);
	$("body").delegate("#btnSuppImgCouv","click",suppImgCouv)
	$("body").delegate(".toSet", "change", setExportValue);
	$("body").delegate( "#formSettings", "submit", validFormSettings);
	$("body").delegate("#btnAnnulerExport", "click", renderProject);
});

function setMenuExport () {
	$("body").html(templates["exportSettings"](project));
};

function setExportValue (e) {
	project.settings[e.currentTarget.id] = $(this).val();
	project.saved = false;
}

function setImgCouv () {
	var file    = document.querySelector('#couvImg').files[0];
	var reader  = new FileReader();

	reader.addEventListener("load", function () {
		project.settings.couvImg = reader.result;
		setMenuExport();
	}, false);

	if (file) reader.readAsDataURL(file);
	project.saved = false;
}

function suppImgCouv() {
	console.log("suppImgCouv");
	project.settings.couvImg = null;
	setMenuExport();
	project.saved = false;
}

function validFormSettings (e) {
	$("body").html(templates["exportEnCours"]());
	setTimeout(exportPdf);
	return false;
};

function exportPdf () {

	var firstPages = [];

	if (project.settings.addCouv) {
		firstPages.push([]);
		if(project.settings.couvImg) firstPages[0].push({
			image: project.settings.couvImg,
			fit: [595.28,841.89],
			absolutePosition: {x: 0, y: 0}
		});
		if(project.settings.couvTitre || project.settings.couvAuteur) firstPages[0].push({
			text: project.settings.couvAuteur,
			absolutePosition: {x: 72, y: 230},
			fontSize: 16
		});
		if(project.settings.couvTitre) firstPages[0].push({
			text: project.settings.couvTitre,
			absolutePosition: {x: 72, y: 250},
			fontSize: 20,
			bold: true
		});
	}

  	generatePdf(firstPages,project.settings.addRules).then(function(contentPdf) {
  		console.log("PDF STRUCTURE", contentPdf);
  		pdfMake.createPdf(contentPdf).download(project.fileName+'.pdf',renderProject);
  	});
  	
}
