# Detective-Playing Game

> (certains disent aussi Dunin-Playing Game)

Ce projet a pour vocation de pouvoir facilement créer des jeux utilisant le DPG system:

Un sorte de jeu de rôle d'enquête pour deux joueurs, mais dans lequel le maître du jeu ne possède pas toutes les informations, et tente de masquer l'identité du coupable.

On peut trouver un exemple [ici](https://dunin.itch.io/dpg).

Ce projet fonctionne avec nwjs, il vous faut donc le télécharger et suivre [ces instructions](http://docs.nwjs.io/en/latest/For%20Users/Getting%20Started/#get-nwjs).
(très simplement, sur windows, faire un glisser-déposer du dossier du projet sur l'application nw.exe fonctionne).

Le logiciel permet de créer tous les documents qui permettent de jouer, une fois renseigné le nom des protagonistes, les questions auquelles ils peuvent répondre, et leurs réponses.
