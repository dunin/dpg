# DPG System - Règles


Nombre de joueurs: 2
Durée: 30 à 60 minutes

## _Attention, vous avez dans les mains un jeu à usage unique, et tout empressement pourrait en dégrader l'expérience! Suivez attentivement ces règles avant de vous lancer dans l'aventure._


## Rôle de chacun des joueurs

Ce jeu propose des rôles différents aux deux joueurs.
L'un endossera celui d'un inspecteur et devra poser une série de questions aux différents suspects afin de découvrir le coupable.
Le second aura le rôle de «maître du jeu» et endossera successivement le rôle de chacun des suspects. Quand il aura à jouer celui du coupable, il devra tenter de brouiller les pistes, afin de perdre le détective...

Choisissez le rôle de chacun des joueurs.
Celui qui a été choisi pour être le «maître du jeu» doit se placer derrière un paravent (ou un grand livre ouvert, ou un paquet de céréales, etc.) afin de cacher aux yeux de l'autre joueur les feuilles qu'il va manipuler. Il prend en main toutes les feuilles qui composent le jeu, et donne à l'autre joueur uniquement «l'introduction de l'inspecteur» et la «table de l'inspecteur». Il garde avec lui l'ensemble des autres feuilles et continue la lecture des règles à haute voix.


## Déroulé d'une partie

Au début de la partie, chacun des joueurs va lire silencieusement sa feuille d'introduction.
Celle du maître du jeu présente l'histoire du crime vécue par le coupable.

Puis, l'inspecteur pourra poser l'une des questions présente sur sa Table de l'inspecteur à l'un des suspects présents (sauf si une croix à l'intersection de la colonne correspondant au nom du suspect et de la ligne de la question lui indique que cela n'est pas possible). Pour cela, il indique au maître du jeu le nom de suspect, le numéro de la question ainsi que l'énoncé de la question .

Si la question est posée à un innocent, le «maître du jeu» va donner la réponse du suspect concerné en lisant le paragraphe correspondant au nombre indiqué à l'intersection entre la colonne correspondant au nom du suspect et la ligne de la question.

_Exemple: L'inspecteur a demandé à madame Bidule de répondre à la question «Avez-vous repris de la salade à midi?», le maître du jeu devra donc répondre en lisant le paragraphe 8._|exemple1.png

Si la question est posée au coupable, le maître du jeu a un choix à faire entre: répondre sincèrement, en lisant le paragraphe indiqué par le nombre foncé (c'est-à-dire de la même couleur que ceux des autres suspects) situé dans la case située à l'intersection entre la colonne correspondant au nom du suspect et la ligne de la question; ou essayer de dissimuler la vérité, en lisant le paragraphe indiqué par le nombre gris présent dans cette case.

_Exemple: L'inspecteur a demandé à monsieur Chouette de répondre à la question «Êtes-vous retourné dans votre chambre après le dîner?». Le maître du jeu sait que monsieur Chouette est coupable, il peut donc répondre sincèrement à cette question en se reportant au paragraphe 1 ou dissimuler la vérité en lisant le paragraphe 7._|exemple2.png

Attention: «dissimuler la vérité» signifie que le coupable va tenter d'improviser un mensonge, ce qui peut l'amener à faire des déclarations contradictoires avec les autres suspects. Si cela est parfois nécessaire pour ne pas être démasqué, certains mensonges peuvent aussi être flagrants. À vous de faire le bon choix, selon ce que vous savez, et les réponses qui ont déjà été données par les autres suspects...
Évidemment, il ne vous est pas possible de consulter les paragraphes correspondants avant de faire votre choix!

Lorsque l'inspecteur a reçu la réponse à sa question, il coche la case correspondante sur la Table de l'inspecteur. Il ne pourra donc plus poser cette question à ce suspect, ni même demandé à ce qu'on lui répète la réponse qu'il vient d'entendre: c'est à lui d'être attentif!


## Précisions

Si une croix est présente sur la table de l'inspecteur ou la table des suspects, cela veut dire que la question ne peut être posée ou que la réponse ne peut être donnée.

_Exemple: L'inspecteur a demandé à monsieur Chouette de répondre à la question «Êtes-vous celui qui a empoisonné mademoiselle Truc?». Une croix indique que Monsieur Chouette ne peut répondre sincèrement. Et c'est mieux ainsi, puisqu'il est coupable._|exemple3.png

Il peut arriver que certaines réponses amènent l'inspecteur à découvrir de nouvelles questions à poser. Dans ce cas, cela est précisé en fin de paragraphe, le maître du jeu le signale alors à l'inspecteur qui ajoute la question sur la Table de l'inspecteur au numéro correspondant.

Enfin, l'inspecteur a le droit de prendre autant de notes qu'il le souhaite.


## Fin du jeu

Lorsque l'inspecteur est certain d'avoir trouvé le coupable, il peut l'accuser, grâce à la dernière ligne de la Table de l'inspecteur. Le maître du jeu répond alors, comme s'il s'agissait d'une question, en lisant le paragraphe indiqué, puis le jeu prend fin par un succès policier ou une terrible erreur judiciaire.



_(Detective-Playing Game ou Dunin-Playing Game est un système crée par dunin, plus d'informations sur dunin.itch.io/dpg)_
